﻿using Logistics.Entity;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
namespace Logistics.Utility
{
    public class CommonUtility
    {
        /// <summary>
        /// Convert object shoft if object is null or string value it will return 0
        /// </summary>
        /// <param name="value">object value</param>
        /// <returns>short value</returns>
        public static short ToInt16(object value)
        {
            short shortValue = 0;
            if (value != null)
            {
                if (short.TryParse(value.ToString(), out shortValue) == true)
                {
                    shortValue = Convert.ToInt16(value.ToString(), CultureInfo.InvariantCulture);
                }
            }

            return shortValue;
        }

        /// <summary>
        /// Convert object integer if object is null or string value it will return 0
        /// </summary>
        /// <param name="value">object value</param>
        /// <returns>integer value</returns>
        public static int ToInt32(object value)
        {
            int intvalue = 0;
            if (value != null)
            {
                if (int.TryParse(value.ToString(), out intvalue) == true)
                {
                    intvalue = Convert.ToInt32(value.ToString(), CultureInfo.InvariantCulture);
                }
            }

            return intvalue;
        }

        /// <summary>
        /// Convert object long if object is null or string value it will return 0
        /// </summary>
        /// <param name="value">object value</param>
        /// <returns>long value</returns>
        public static long ToInt64(object value)
        {
            long longvalue = 0;
            if (value != null)
            {
                if (long.TryParse(value.ToString(), out longvalue) == true)
                {
                    longvalue = Convert.ToInt64(value.ToString(), CultureInfo.InvariantCulture);
                }
            }

            return longvalue;
        }

        /// <summary>
        /// Convert object to boolean if object is null or string value it will return false
        /// </summary>
        /// <param name="value">object value</param>
        /// <returns>true or false</returns>
        public static bool ToBoolean(object value)
        {
            bool boolvalue = false;
            if (value != null)
            {
                if (bool.TryParse((value.ToString() == "1" ? "true" : value.ToString()), out boolvalue) == true)
                {
                    value = value.ToString() == "1" ? "true" : value.ToString();
                    boolvalue = Convert.ToBoolean(value, CultureInfo.InvariantCulture);
                }
            }

            return boolvalue;
        }

        /// <summary>
        /// Convert object String if object is null or string value it will return Empty
        /// </summary>
        /// <param name="value">object value</param>
        /// <returns>integer value</returns>
        public static string ToString(object value)
        {
            string stringvalue = string.Empty;
            if (value != null)
                stringvalue = Convert.ToString(value.ToString(), CultureInfo.InvariantCulture);
            return stringvalue;
        }

        /// <summary>
        /// Convert object to datetime if object is null it will return null
        /// </summary>
        /// <param name="value">object value</param>
        /// <returns>DateTime value</returns>

        public static Nullable<System.DateTime> ToDateTime(object value)
        {
            Nullable<System.DateTime> dt = null;

            if (!string.IsNullOrEmpty(Convert.ToString(value)))
                dt = Convert.ToDateTime(Convert.ToString(value));

            return dt;
        }

        /// <summary>
        /// This function is used to convert the UTC Time into any Standard Time Zone
        /// </summary>
        public static DateTime ConvertUTCtoTimeZone(DateTime DateTimeUTC, String StandardName)
        {
            try
            {
                if (string.IsNullOrEmpty(StandardName))
                {
                    StandardName = "India Standard Time";
                }
                else if (StandardName.Length < 5)
                {
                    StandardName = "India Standard Time";
                }


                TimeZoneInfo CZONE = TimeZoneInfo.FindSystemTimeZoneById(StandardName);
                DateTime ConvertedTime = TimeZoneInfo.ConvertTimeFromUtc(DateTimeUTC, CZONE);
                return ConvertedTime;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This function is used to convert the Standard Time Zone to UTC Time
        /// </summary>
        public static DateTime ConvertTimeZonetoUTC(DateTime CDateTime, String StandardName)
        {
            try
            {
                if (string.IsNullOrEmpty(StandardName))
                {
                    StandardName = "India Standard Time";
                }
                else if (StandardName.Length < 5)
                {
                    StandardName = "India Standard Time";
                }

                TimeZoneInfo CZONE = TimeZoneInfo.FindSystemTimeZoneById(StandardName);
                DateTime ConvertedTime = TimeZoneInfo.ConvertTimeToUtc(CDateTime, CZONE);
                return ConvertedTime;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get calculated Date based on configuration master
        /// </summary>
        /// <returns>Datetime</returns>
        public static DateTime GetDefaultDateByID(DateTime currentDate, int dateDefaultId, string param)
        {
            DateTime currentdate = currentDate;
            DateTime calculateddate = currentdate;
            int rYear = currentDate.Year;
            int rMonth = currentDate.Month;
            int rDay = currentDate.Day;

            switch (dateDefaultId)
            {
                case 1:
                    calculateddate = currentdate;
                    break;

                case 2:
                    calculateddate = (new DateTime(rYear, rMonth, rDay)).AddDays(1).AddTicks(-1);
                    break;

                case 3:
                    calculateddate = new DateTime(rYear, rMonth, 1);
                    break;

                case 4:
                    calculateddate = new DateTime(rYear, rMonth, DateTime.DaysInMonth(rYear, rMonth));
                    break;

                case 5:
                    calculateddate = currentdate;
                    break;

                case 6:
                    calculateddate = currentdate.AddDays(CommonUtility.ToInt32(param));
                    break;

                case 7:
                    calculateddate = currentdate.AddDays(-(CommonUtility.ToInt32(param)));
                    break;
                case 11:
                case 8:
                    calculateddate = currentDate;
                    break;
                case 12:
                case 9:
                    int hours, min = 0;
                    if (!string.IsNullOrEmpty(param) && param != "00:00")
                    {
                        string[] hoursmin = param.Split(':');
                        hours = CommonUtility.ToInt32(hoursmin[0]);
                        if (hoursmin.Length == 2)
                            min = CommonUtility.ToInt32(hoursmin[1]);
                        calculateddate = currentDate.AddHours(hours).AddMinutes(min);
                    }
                    else
                    {
                        calculateddate = currentDate;
                    }

                    break;
                case 13:
                case 10:
                    int chours, cmin = 0;
                    if (!string.IsNullOrEmpty(param) && param != "00:00")
                    {
                        string[] hoursmin = param.Split(':');
                        chours = CommonUtility.ToInt32(hoursmin[0]);
                        if (hoursmin.Length == 2)
                            cmin = CommonUtility.ToInt32(hoursmin[1]);
                        calculateddate = currentDate.AddHours(-chours).AddMinutes(-cmin);
                    }
                    else
                    {
                        calculateddate = currentDate;
                    }
                    break;

                default:
                    calculateddate = currentDate;
                    break;
            }

            return calculateddate;
        }

        //public static string PascalCase(string str)
        //{
        //    if (!string.IsNullOrEmpty(str))
        //    {
        //        TextInfo cultInfo = new CultureInfo("en-US", false).TextInfo;
        //        str = Regex.Replace(str, "([A-Z]+)", "$1");
        //        str = cultInfo.ToTitleCase(str);
        //    }
        //    else
        //        str = string.Empty;
        //    return str;
        //}

        public static AccessToken GenerateAccessToken(Login cb, List<AppSettings> objAppSet)
        {
            var usersClaims = new[]{
                                    new Claim("LoginId", cb.LoginID),
                                    new Claim("FirstName", cb.FirstName),
                                    new Claim("LastName", cb.LastName)
                                };

            string JWTTokenTime = objAppSet.Find(bn => bn.KeyName == "JWTTokenTime").KeyValue;
            string TokenKey = objAppSet.Find(bn => bn.KeyName == "JWTToken").KeyValue;
            string AppNameKey = objAppSet.Find(bn => bn.KeyName == "JWTTokenAppName").KeyValue;

            string hashkey = GenerateSHA512String(TokenKey + AppNameKey);
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(hashkey));
            DateTime expireTime = DateTime.UtcNow.AddMinutes(Convert.ToInt32(JWTTokenTime));
            var jwtToken = new JwtSecurityToken(issuer: "DAS",
                audience: "Anyone",
                claims: usersClaims,
                notBefore: DateTime.UtcNow,
                expires: expireTime,
                signingCredentials: new SigningCredentials(key, SecurityAlgorithms.HmacSha256)
            );
            return new AccessToken(new JwtSecurityTokenHandler().WriteToken(jwtToken), expireTime);
        }

        public static string GenerateSHA512String(string inputString)
        {
            SHA512 sha512 = SHA512Managed.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(inputString);
            byte[] hash = sha512.ComputeHash(bytes);
            return GetStringFromHash(hash);
        }

        public static string GetStringFromHash(byte[] hash)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                result.Append(hash[i].ToString("X2"));
            }
            return result.ToString();
        }       
    }

    public class EncryptionLibrary
    {
        private static string _password = "D@$7$2Ooo$F@A2!";
        private static byte[] _salt = new byte[] { 5, 1, 6, 0, 2, 1, 7, 8 }; //"D@$7$2Ooo$F@A2!"
        private static byte[] AES_Encrypt(byte[] bytesToBeEncrypted, byte[] passwordBytes)
        {
            byte[] encryptedBytes = null;

            // Set your salt here, change it to meet your flavor:
            // The salt bytes must be at least 8 bytes.
            byte[] saltBytes = _salt; // Encoding.UTF8.GetBytes(_salt);

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeEncrypted, 0, bytesToBeEncrypted.Length);
                        cs.Close();
                    }
                    encryptedBytes = ms.ToArray();
                }
            }
            return encryptedBytes;
        }

        private static byte[] AES_Decrypt(byte[] bytesToBeDecrypted, byte[] passwordBytes)
        {
            byte[] decryptedBytes = null;

            byte[] saltBytes = _salt; // Encoding.UTF8.GetBytes(_salt);

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeDecrypted, 0, bytesToBeDecrypted.Length);
                        cs.Close();
                    }
                    decryptedBytes = ms.ToArray();
                }
            }

            return decryptedBytes;
        }
        public static string EncryptText(string input)
        {
            // Get the bytes of the string
            byte[] bytesToBeEncrypted = Encoding.UTF8.GetBytes(input);
            byte[] passwordBytes = Encoding.UTF8.GetBytes(_password);
            // Hash the password with SHA256
            //passwordBytes = SHA256.Create().ComputeHash(passwordBytes);
            var hashmd5 = new MD5CryptoServiceProvider();
            passwordBytes = hashmd5.ComputeHash(Encoding.UTF8.GetBytes(_password));

            byte[] bytesEncrypted = AES_Encrypt(bytesToBeEncrypted, passwordBytes);

            string result = Convert.ToBase64String(bytesEncrypted);

            return result;
        }

        public static string DecryptText(string input)
        {
            // Get the bytes of the string
            byte[] bytesToBeDecrypted = Convert.FromBase64String(input);
            byte[] passwordBytes = Encoding.UTF8.GetBytes(_password);
            //passwordBytes = SHA256.Create().ComputeHash(passwordBytes);
            var hashmd5 = new MD5CryptoServiceProvider();
            passwordBytes = hashmd5.ComputeHash(Encoding.UTF8.GetBytes(_password));

            byte[] bytesDecrypted = AES_Decrypt(bytesToBeDecrypted, passwordBytes);

            string result = Encoding.UTF8.GetString(bytesDecrypted);

            return result;
        }

    }
}
