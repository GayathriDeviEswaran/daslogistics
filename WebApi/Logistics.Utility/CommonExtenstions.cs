﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Logistics.Utility
{
   public static class CommonExtenstions
    {
        public static string MToLower(this object value)
        {
            return value.ToString().ToLowerInvariant();
        }
        public static string MToUpper(this object value)
        {
            return value.ToString().ToUpperInvariant();// ToLower(CultureInfo.InvariantCulture);
        }

        public static string ToXmlString(this object o)
        {
            //StringWriter sw = new StringWriter();

            MemoryStream strm = new MemoryStream();
            string sw = string.Empty;
            try
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.OmitXmlDeclaration = true;

                using (XmlWriter writer = XmlWriter.Create(strm, settings))
                {
                    XmlSerializer serializer = new XmlSerializer(o.GetType());
                    serializer.Serialize(writer, o);
                    strm.Position = 0;
                    StreamReader reader = new StreamReader(strm);
                    sw = reader.ReadLine();

                }
            }
            catch (Exception ex)
            {
                //Handle Exception Code
            }
            finally
            {
                strm.Close();
            }
            return sw.ToString();
        }

        public static string[] StrSplit(this string str, string splitter)
        {
            return str.Split(new[] { splitter }, StringSplitOptions.None);
        }
    }
}
