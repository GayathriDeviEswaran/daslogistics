﻿using Logistics.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistics.BusinessAccessInterface
{
    public interface INotification
    {
        IEnumerable<NotificationTemplate> GetNotificationByProcessID(Notification ProcessID);
        IEnumerable<NotificationTemplate> GetNotificationByID(Notification ID);
        IEnumerable<NotificationTemplate> AddNotification(NotificationTemplate data);
    }
}
