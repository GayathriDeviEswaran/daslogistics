﻿using Logistics.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistics.BusinessAccessInterface
{
    public interface ILogin
    {
        ResponseLogin ValidateLogin(string username, string password);
    }
}
