﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logistics.Entity;
namespace Logistics.BusinessAccessInterface
{
    public interface ISubrubs
    {
        IEnumerable<SubrupsEntity> GetSubrupsDetailsbyID(SubrupsID ID);
    }
}
