﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logistics.Entity;

namespace Logistics.BusinessAccessInterface
{
    public interface IMailNotification
    {

        IEnumerable<MailNotificationTemplate> GetMailNotificationByProcessID(MailNotification ProcessID);
        IEnumerable<MailNotificationTemplate> GetMailNotificationByID(MailNotification ID);
        IEnumerable<MailNotificationTemplate> AddMailNotification(MailNotificationTemplate data);

    }
}
