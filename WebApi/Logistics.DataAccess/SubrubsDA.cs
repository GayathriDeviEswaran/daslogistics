﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logistics.DataAccessInterface;
using Logistics.Entity;
using Logistics.Utility;
using System.Data;
namespace Logistics.DataAccess
{
    public class SubrubsDA: DBbase,ISubrupsDA
    {
        public IEnumerable<SubrupsEntity> GetSubrupsDetailsbyID(SubrupsID SubrupID)
        {
            DBInitialize("usp_GetSubrups");
            DatabaseName.AddInParameter(DBBaseCommand, "@ID", DbType.Int32, SubrupID.ID);
            List<SubrupsEntity> lst_SubrubsDetails = new List<SubrupsEntity>();
            SubrupsEntity obj_SubrubsDetails = new SubrupsEntity();
            using (IDataReader objIDataReader = DatabaseName.ExecuteReader(DBBaseCommand))
            {
                while (objIDataReader.Read())
                {
                    obj_SubrubsDetails = new SubrupsEntity();
                    obj_SubrubsDetails.id = CommonUtility.ToInt32(objIDataReader["Id"]);
                    obj_SubrubsDetails.Subrub = CommonUtility.ToString(objIDataReader["Subrub"]);
                    obj_SubrubsDetails.State = CommonUtility.ToString(objIDataReader["State"]);
                    obj_SubrubsDetails.Postcode = CommonUtility.ToInt32(objIDataReader["Postcode"]);
                    obj_SubrubsDetails.DepotsAndRun = CommonUtility.ToString(objIDataReader["DepotsAndRun"]);
                    obj_SubrubsDetails.Longitude = CommonUtility.ToString(objIDataReader["Longitude"]);
                    obj_SubrubsDetails.Latitude = CommonUtility.ToString(objIDataReader["Latitude"]);
                    obj_SubrubsDetails.Status = CommonUtility.ToInt32(objIDataReader["Status"]);

                    lst_SubrubsDetails.Add(obj_SubrubsDetails);
                }
            }
            return lst_SubrubsDetails;


        }
    }
}
