﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logistics.Entity;
using Logistics.Utility;
using Logistics.DataAccessInterface;
using System.Data;
namespace Logistics.DataAccess
{
    public class AccountTermsDA:DBbase, IAccountTermsDA
    {
        public IEnumerable<AccountTerms> GetAccountTerms(AccountTerms data)
        {
            DBInitialize("usp_GetAccountTerms");
            List<AccountTerms> lst_AccountTerms = new List<AccountTerms>();
            AccountTerms obj_AccountTermss = new AccountTerms();
            using (IDataReader objIDataReader = DatabaseName.ExecuteReader(DBBaseCommand))
            {
                while (objIDataReader.Read())
                {
                    obj_AccountTermss = new AccountTerms();
                    lst_AccountTerms.Add(obj_AccountTermss);
                }
            }
            return lst_AccountTerms;
        }

    }
}
