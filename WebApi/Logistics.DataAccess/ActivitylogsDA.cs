﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logistics.DataAccessInterface;
using Logistics.Entity;
using Logistics.Utility;
using System.Data;
namespace Logistics.DataAccess
{
    public class ActivitylogsDA : DBbase, IActivitylogsDA
    {
        public IEnumerable<Activitylogs> AddActivityLogs(Activitylogs data)
        {
            DBInitialize("usp_AddActivityLogs");
            DatabaseName.AddInParameter(DBBaseCommand, "@MailNotificationTemplateID", DbType.Int32, data.UserID);
            DatabaseName.AddInParameter(DBBaseCommand, "@MailNotification_Template_Name", DbType.String, data.FunctionPerformed);
            DatabaseName.AddInParameter(DBBaseCommand, "@MailNotification_Message", DbType.String, data.URL);
            DatabaseName.AddInParameter(DBBaseCommand, "@MailNotification_TypeID", DbType.Int32, data.IPAddress);
            List<Activitylogs> lst_Activitylogs = new List<Activitylogs>();
            Activitylogs obj_Activitylogs = new Activitylogs();
            using (IDataReader objIDataReader = DatabaseName.ExecuteReader(DBBaseCommand))
            {
                while (objIDataReader.Read())
                {
                    obj_Activitylogs = new Activitylogs();
                    lst_Activitylogs.Add(obj_Activitylogs);
                }
            }
            return lst_Activitylogs;
        }
        
    }
}
