﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logistics.Entity;
using Logistics.DataAccessInterface;
using Logistics.Utility;
using System.Data;
namespace Logistics.DataAccess
{
   public class ZonesDA: DBbase, IZonesDA
    {
        public IEnumerable<Zone> AddZones(Zone data) {
            DBInitialize("usp_AddZones");
            DatabaseName.AddInParameter(DBBaseCommand, "@Zone_no", DbType.String, data.Zone_no);
            DatabaseName.AddInParameter(DBBaseCommand, "@Zone_Name", DbType.String, data.Zone_Name);
            DatabaseName.AddInParameter(DBBaseCommand, "@Zone_Prefix", DbType.String, data.Zone_Prefix);
            DatabaseName.AddInParameter(DBBaseCommand, "@Status", DbType.Int32, data.Status);
            DatabaseName.AddInParameter(DBBaseCommand, "@UserID", DbType.Int32, data.UserID);

            List<Zone> lst_Zonelogs = new List<Zone>();
            Zone obj_Zonelogs = new Zone();
            using (IDataReader objIDataReader = DatabaseName.ExecuteReader(DBBaseCommand))
            {
                while (objIDataReader.Read())
                {
                    obj_Zonelogs = new Zone();
                    lst_Zonelogs.Add(obj_Zonelogs);
                }
            }
            return lst_Zonelogs;
        }
    }
}
