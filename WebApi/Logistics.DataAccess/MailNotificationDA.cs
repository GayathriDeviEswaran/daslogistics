﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logistics.DataAccessInterface;
using Logistics.Entity;
using Logistics.Utility;
using System.Data;
namespace Logistics.DataAccess
{
    public class MailNotificationDA : DBbase, IMailNotificationDA
    {
        public IEnumerable<MailNotificationTemplate> GetMailNotificationByProcessID(MailNotification ProcessID)
        {
            DBInitialize("usp_GetMailNotificationByProcessID");
            DatabaseName.AddInParameter(DBBaseCommand, "@ProcessID", DbType.Int32, ProcessID.ProcessID);
            List<MailNotificationTemplate> lst_Notification = new List<MailNotificationTemplate>();
            MailNotificationTemplate obj_Notification = new MailNotificationTemplate();
            using (IDataReader objIDataReader = DatabaseName.ExecuteReader(DBBaseCommand))
            {
                while (objIDataReader.Read())
                {
                    obj_Notification = new MailNotificationTemplate();
                    obj_Notification.MailNotificationTemplateID = CommonUtility.ToInt32(objIDataReader["MailNotificationTemplateID"]);
                    obj_Notification.MailNotification_Template_Name = CommonUtility.ToString(objIDataReader["MailNotification_Template_Name"]);
                    obj_Notification.MailNotification_Message = CommonUtility.ToString(objIDataReader["MailNotification_Message"]);
                    obj_Notification.ProcessID = CommonUtility.ToInt32(objIDataReader["ProcessID"]);
                    obj_Notification.RStatus = CommonUtility.ToInt32(objIDataReader["RStatus"]);
                    obj_Notification.MailNotification_TypeID = CommonUtility.ToInt32(objIDataReader["MailNotification_TypeID"]);
                    obj_Notification.IsActive = CommonUtility.ToInt32(objIDataReader["IsActive"]);

                    lst_Notification.Add(obj_Notification);
                }
            }
            return lst_Notification;

        }

        public IEnumerable<MailNotificationTemplate> GetMailNotificationByID(MailNotification ID)
        {
            DBInitialize("usp_GetMailNotificationByID");
            DatabaseName.AddInParameter(DBBaseCommand, "@MailNotificationTemplateID", DbType.Int32,  ID.ProcessID);
            List<MailNotificationTemplate> lst_Notification = new List<MailNotificationTemplate>();
            MailNotificationTemplate obj_Notification = new MailNotificationTemplate();
            using (IDataReader objIDataReader = DatabaseName.ExecuteReader(DBBaseCommand))
            {
                while (objIDataReader.Read())
                {
                    obj_Notification = new MailNotificationTemplate();
                    obj_Notification.MailNotificationTemplateID = CommonUtility.ToInt32(objIDataReader["MailNotificationTemplateID"]);
                    obj_Notification.MailNotification_Template_Name = CommonUtility.ToString(objIDataReader["MailNotification_Template_Name"]);
                    obj_Notification.MailNotification_Message = CommonUtility.ToString(objIDataReader["MailNotification_Message"]);
                    obj_Notification.ProcessID = CommonUtility.ToInt32(objIDataReader["ProcessID"]);
                    obj_Notification.RStatus = CommonUtility.ToInt32(objIDataReader["RStatus"]);
                    obj_Notification.MailNotification_TypeID = CommonUtility.ToInt32(objIDataReader["MailNotification_TypeID"]);
                    obj_Notification.IsActive = CommonUtility.ToInt32(objIDataReader["IsActive"]);

                    lst_Notification.Add(obj_Notification);
                }
            }
            return lst_Notification;

        }

        public IEnumerable<MailNotificationTemplate> AddMailNotification(MailNotificationTemplate data)
        {
            DBInitialize("usp_AddMailNotification");
            DatabaseName.AddInParameter(DBBaseCommand, "@MailNotificationTemplateID", DbType.Int32, data.MailNotificationTemplateID);
            DatabaseName.AddInParameter(DBBaseCommand, "@MailNotification_Template_Name", DbType.String, data.MailNotification_Template_Name);
            DatabaseName.AddInParameter(DBBaseCommand, "@MailNotification_Message", DbType.String, data.MailNotification_Message);
            DatabaseName.AddInParameter(DBBaseCommand, "@MailNotification_TypeID", DbType.Int32, data.MailNotification_TypeID);
            DatabaseName.AddInParameter(DBBaseCommand, "@ProcessID", DbType.Int32, data.ProcessID);
            DatabaseName.AddInParameter(DBBaseCommand, "@RStatus", DbType.Int32, data.RStatus);
            DatabaseName.AddInParameter(DBBaseCommand, "@IsActive", DbType.Boolean, data.IsActive);
            DatabaseName.AddInParameter(DBBaseCommand, "@UserID", DbType.Int32, data.UserID);
            List<MailNotificationTemplate> lst_Notification = new List<MailNotificationTemplate>();
            MailNotificationTemplate obj_Notification = new MailNotificationTemplate();
            using (IDataReader objIDataReader = DatabaseName.ExecuteReader(DBBaseCommand))
            {
                while (objIDataReader.Read())
                {
                    obj_Notification = new MailNotificationTemplate();
                    //obj_Notification.NotificationTemplateID = CommonUtility.ToInt32(objIDataReader["NotificationTemplateID"]);
                    //obj_Notification.NotificationTemplateID = CommonUtility.ToInt32(objIDataReader["Notification_Template_Name"]);
                    //obj_Notification.NotificationTemplateID = CommonUtility.ToInt32(objIDataReader["Notification_Message"]);
                    //obj_Notification.NotificationTemplateID = CommonUtility.ToInt32(objIDataReader["ProcessID"]);
                    //obj_Notification.NotificationTemplateID = CommonUtility.ToInt32(objIDataReader["Status"]);
                    //obj_Notification.NotificationTemplateID = CommonUtility.ToInt32(objIDataReader["Notification_TypeID"]);

                    lst_Notification.Add(obj_Notification);
                }
            }
            return lst_Notification;
        }

    }
}
