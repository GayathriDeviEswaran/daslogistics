﻿using Logistics.DataAccessInterface;
using Logistics.Entity;
using Logistics.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistics.DataAccess
{
    public class LoginDA : DBbase, ILoginDA
    {
        public ResponseLogin ValidateLogin(string username, string password)
        {
            ResponseLogin R1 = new ResponseLogin();
            DBInitialize("[dbo].[ValidateLogin]");
            DatabaseName.AddInParameter(DBBaseCommand, "@UserName", DbType.String, username);
            DatabaseName.AddInParameter(DBBaseCommand, "@Password", DbType.String, password);
            using (IDataReader objIDataReader = DatabaseName.ExecuteReader(DBBaseCommand))
            {
                R1.MenuMasters = new List<MenuMaster>();
                while (objIDataReader.Read())
                {
                    MenuMaster M = new MenuMaster();
                    M.ID = CommonUtility.ToInt32(objIDataReader["ID"]);
                    M.ParentID = CommonUtility.ToInt32(objIDataReader["ParentID"]);
                    M.DisplayName = CommonUtility.ToString(objIDataReader["DisplayName"]);
                    M.MenuIcon = CommonUtility.ToString(objIDataReader["MenuIcon"]);
                    M.URL = CommonUtility.ToString(objIDataReader["URL"]);
                    M.DisplayOrder = CommonUtility.ToInt32(objIDataReader["DisplayOrder"]);
                    M.status = CommonUtility.ToInt32(objIDataReader["status"]);
                    R1.MenuMasters.Add(M);
                }
                objIDataReader.NextResult();
                while (objIDataReader.Read())
                {
                    R1.Role = new RoleMaster();
                    R1.Role.ID= CommonUtility.ToInt32(objIDataReader["ParentID"]);
                    R1.Role.RoleName = CommonUtility.ToString(objIDataReader["DisplayName"]);
                    R1.Role.status = CommonUtility.ToInt32(objIDataReader["status"]);
                }
                R1.UserName = username;
                if(R1.MenuMasters.Count==0)
                {
                    R1.Errors = new Error { ErrorMessage = "Invalid", status = false };
                }
                else
                {
                    R1.Errors = new Error { ErrorMessage = "Success", status = true };
                }                 
            }
            return R1;
        }
    }
}
