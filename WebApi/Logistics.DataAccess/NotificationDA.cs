﻿using Logistics.DataAccessInterface;
using Logistics.Entity;
using Logistics.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Logistics.DataAccess
{
   public class NotificationDA: DBbase, INotificationDA 
    {
        public IEnumerable<NotificationTemplate> GetNotificationByProcessID(Notification ProcessID)
        {
            DBInitialize("usp_GetNotificationByProcessID");
            DatabaseName.AddInParameter(DBBaseCommand, "@ProcessID", DbType.Int32, ProcessID.ProcessID);
            List<NotificationTemplate> lst_Notification = new List<NotificationTemplate>();
            NotificationTemplate obj_Notification = new NotificationTemplate();
            using (IDataReader objIDataReader = DatabaseName.ExecuteReader(DBBaseCommand))
            {
                while (objIDataReader.Read())
                {
                    obj_Notification = new NotificationTemplate();
                    obj_Notification.NotificationTemplateID = CommonUtility.ToInt32(objIDataReader["NotificationTemplateID"]);
                    obj_Notification.Notification_Template_Name = CommonUtility.ToString(objIDataReader["Notification_Template_Name"]);
                    obj_Notification.Notification_Message = CommonUtility.ToString(objIDataReader["Notification_Message"]);
                    obj_Notification.ProcessID = CommonUtility.ToInt32(objIDataReader["ProcessID"]);
                    obj_Notification.Status = CommonUtility.ToInt32(objIDataReader["Status"]);
                    obj_Notification.Notification_TypeID = CommonUtility.ToInt32(objIDataReader["Notification_TypeID"]);
                    obj_Notification.IsActive = CommonUtility.ToInt32(objIDataReader["IsActive"]);

                    lst_Notification.Add(obj_Notification);
                }
            }
            return lst_Notification;
        }
        public IEnumerable<NotificationTemplate> GetNotificationByID(Notification ID)
        {
            DBInitialize("usp_GetNotificationByID");
            DatabaseName.AddInParameter(DBBaseCommand, "@NotificationTemplateID", DbType.Int32, ID.ProcessID);
            List<NotificationTemplate> lst_Notification = new List<NotificationTemplate>();
            NotificationTemplate obj_Notification = new NotificationTemplate();
            using (IDataReader objIDataReader = DatabaseName.ExecuteReader(DBBaseCommand))
            {
                while (objIDataReader.Read())
                {
                    obj_Notification = new NotificationTemplate();
                    obj_Notification.NotificationTemplateID = CommonUtility.ToInt32(objIDataReader["NotificationTemplateID"]);
                    obj_Notification.Notification_Template_Name = CommonUtility.ToString(objIDataReader["Notification_Template_Name"]);
                    obj_Notification.Notification_Message = CommonUtility.ToString(objIDataReader["Notification_Message"]);
                    obj_Notification.ProcessID = CommonUtility.ToInt32(objIDataReader["ProcessID"]);
                    obj_Notification.Status = CommonUtility.ToInt32(objIDataReader["Status"]);
                    obj_Notification.Notification_TypeID = CommonUtility.ToInt32(objIDataReader["Notification_TypeID"]);
                    obj_Notification.IsActive = CommonUtility.ToInt32(objIDataReader["IsActive"]);
                    lst_Notification.Add(obj_Notification);
                }
            }
            return lst_Notification;
        }

        public IEnumerable<NotificationTemplate> AddNotification(NotificationTemplate data)
        {
            DBInitialize("usp_AddNotification");
            DatabaseName.AddInParameter(DBBaseCommand, "@NotificationTemplateID", DbType.Int32, data.NotificationTemplateID);
            DatabaseName.AddInParameter(DBBaseCommand, "@Notification_Template_Name", DbType.String, data.Notification_Template_Name);
            DatabaseName.AddInParameter(DBBaseCommand, "@Notification_Message", DbType.String, data.Notification_Message);
            DatabaseName.AddInParameter(DBBaseCommand, "@Notification_TypeID", DbType.Int32, data.Notification_TypeID);
            DatabaseName.AddInParameter(DBBaseCommand, "@ProcessID", DbType.Int32, data.ProcessID);
            DatabaseName.AddInParameter(DBBaseCommand, "@Status", DbType.Int32, data.Status);
            DatabaseName.AddInParameter(DBBaseCommand, "@IsActive", DbType.Boolean, data.IsActive);
            DatabaseName.AddInParameter(DBBaseCommand, "@UserID", DbType.Int32, data.UserID);
            List<NotificationTemplate> lst_Notification = new List<NotificationTemplate>();
            NotificationTemplate obj_Notification = new NotificationTemplate();
            using (IDataReader objIDataReader = DatabaseName.ExecuteReader(DBBaseCommand))
            {
                while (objIDataReader.Read())
                {
                    obj_Notification = new NotificationTemplate();
                    //obj_Notification.NotificationTemplateID = CommonUtility.ToInt32(objIDataReader["NotificationTemplateID"]);
                    //obj_Notification.NotificationTemplateID = CommonUtility.ToInt32(objIDataReader["Notification_Template_Name"]);
                    //obj_Notification.NotificationTemplateID = CommonUtility.ToInt32(objIDataReader["Notification_Message"]);
                    //obj_Notification.NotificationTemplateID = CommonUtility.ToInt32(objIDataReader["ProcessID"]);
                    //obj_Notification.NotificationTemplateID = CommonUtility.ToInt32(objIDataReader["Status"]);
                    //obj_Notification.NotificationTemplateID = CommonUtility.ToInt32(objIDataReader["Notification_TypeID"]);

                    lst_Notification.Add(obj_Notification);
                }
            }
            return lst_Notification;
        }


    }
}
