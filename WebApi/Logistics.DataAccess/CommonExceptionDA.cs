﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logistics.DataAccessInterface;
using Logistics.Entity;
using Logistics.ExceptionHandling;
namespace Logistics.DataAccess
{
    public class CommonExceptionDA:ICommonExceptionDA
    {
        public void AddErrorlog(CommonErrorException error)
        {
           // var _response = new CommonErrorException();
          CommonException.SendErrorToText(error.ex, error.hostIp);

        }
        public void Writelog(CommonErrorException error)
        {
            // var _response = new CommonErrorException();
            CommonException.SendErrorToText(error.ex, error.hostIp);

        }
    }
}
