﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logistics.Utility;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Logistics.DataAccess
{
    public class DBbase
    {
        public Database DatabaseName { get; set; }
        public DbCommand DBBaseCommand { get; set; }
        protected void DBInitialize(string storedProcedure)
        {
            string ConStr = ConfigurationManager.AppSettings["ConStr"].ToString();
            InitializeDatabase(storedProcedure, ConStr);            
        }
        private void InitializeDatabase(string storedProcedure, string Connection)
        {
            if (!string.IsNullOrEmpty(Connection))
            {
                DatabaseName = new Microsoft.Practices.EnterpriseLibrary.Data.Sql.SqlDatabase(Connection);
                DbConnection objCon = DatabaseName.CreateConnection();
                try
                {
                    objCon.Open();
                    DBBaseCommand = DatabaseName.GetStoredProcCommand(storedProcedure);
                    var settingsValue = ConfigurationManager.AppSettings["CmdTimeOut"];
                    if (!string.IsNullOrEmpty(settingsValue))
                        DBBaseCommand.CommandTimeout = CommonUtility.ToInt32(settingsValue);
                    objCon.Close();
                }
                catch (Exception Ex)
                {
                
                }
                finally
                {
                    if (objCon != null)
                        objCon.Close();

                }
            }
        }
    }
   
}
