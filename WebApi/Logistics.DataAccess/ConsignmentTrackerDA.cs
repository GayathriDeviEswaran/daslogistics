﻿using Logistics.DataAccessInterface;
using Logistics.Entity;
using Logistics.Utility;
using System;
using System.Collections.Generic;
using System.Data;

namespace Logistics.DataAccess
{
    public class ConsignmentTrackerDA : DBbase, IConsignmentTrackerDA
    {
        public IEnumerable<Tracking> GetInvoiceByConNote(Consignment consignment)
        {
            DBInitialize("spTrackInvoiceByConNote");
            DatabaseName.AddInParameter(DBBaseCommand, "@ConNoteNumber", DbType.Int32, consignment.ConsignmentNumber);
            List<Tracking> Track = new List<Tracking>();
            Tracking tracking = new Tracking();
            using (IDataReader objIDataReader = DatabaseName.ExecuteReader(DBBaseCommand))
            {
                while (objIDataReader.Read())
                {
                    tracking = new Tracking();
                    tracking.ConsignmentNumber = CommonUtility.ToString(objIDataReader["ConsignmentNumber"]);
                    tracking.ConsignmentDate = CommonUtility.ToDateTime(objIDataReader["ConsignmentDate"]);
                    tracking.StatusUpdatedDate = CommonUtility.ToDateTime(objIDataReader["StatusUpdatedDate"]);
                    tracking.StatusUpdatedTime = CommonUtility.ToDateTime(objIDataReader["StatusUpdatedTime"]);
                    tracking.ImagePath = CommonUtility.ToString(objIDataReader["ImagePath"]);
                    tracking.ReceivedImage = CommonUtility.ToString(objIDataReader["ReceivedImage"]);
                    tracking.ReferenceData = CommonUtility.ToString(objIDataReader["ReferenceData"]);
                    tracking.StatusDescription = CommonUtility.ToString(objIDataReader["StatusDescription"]);
                    tracking.StatusIdentifier = CommonUtility.ToString(objIDataReader["StatusIdentifier"]);
                    Track.Add(tracking);
                }
            }
            return Track;
        }
        /// <summary>
        /// Returns the entire list of consignments with status.
        /// </summary>
        /// <param name="consignment"></param>
        public IEnumerable<Tracking> GetConsignmentTracker(Consignment consignment)
        {
            DBInitialize("spTrackDetailsByConNote");
            DatabaseName.AddInParameter(DBBaseCommand, "@ConNoteNumber", DbType.Int32, consignment.ConsignmentNumber);
            List<Tracking> Track = new List<Tracking>();
            Tracking tracking = new Tracking();
            using (IDataReader objIDataReader = DatabaseName.ExecuteReader(DBBaseCommand))
            {
                while (objIDataReader.Read())
                {
                    tracking = new Tracking();
                    tracking.ConsignmentNumber = CommonUtility.ToString(objIDataReader["ConsignmentNumber"]);
                    tracking.ConsignmentDate = CommonUtility.ToDateTime(objIDataReader["ConsignmentDate"]);
                    tracking.StatusUpdatedDate = CommonUtility.ToDateTime(objIDataReader["StatusUpdatedDate"]);
                    tracking.StatusUpdatedTime = CommonUtility.ToDateTime(objIDataReader["StatusUpdatedTime"]);
                    tracking.ImagePath = CommonUtility.ToString(objIDataReader["ImagePath"]);
                    tracking.ReceivedImage = CommonUtility.ToString(objIDataReader["ReceivedImage"]);
                    tracking.ReferenceData = CommonUtility.ToString(objIDataReader["ReferenceData"]);
                    tracking.StatusDescription = CommonUtility.ToString(objIDataReader["StatusDescription"]);
                    tracking.StatusIdentifier = CommonUtility.ToString(objIDataReader["StatusIdentifier"]);
                    Track.Add(tracking);
                }
            }
            return Track;
        }

        /// <SUMMARY>
        /// Checks if Consignment is delivered for consignment, and returns the status with data.
        /// </SUMMARY>
        /// <param name="consignment"></param>
        public IEnumerable<Tracking> GetItemDeliveredDetail(Consignment consignment)
        {
            DBInitialize("spTrackByDeliveryConNote");
            DatabaseName.AddInParameter(DBBaseCommand, "@ConNoteNumber", DbType.Int32, consignment.ConsignmentNumber);
            List<Tracking> Track = new List<Tracking>();
            Tracking tracking = new Tracking();
            using (IDataReader objIDataReader = DatabaseName.ExecuteReader(DBBaseCommand))
            {
                while (objIDataReader.Read())
                {
                    tracking = new Tracking();
                    tracking.ConsignmentNumber = CommonUtility.ToString(objIDataReader["ConsignmentNumber"]);
                    tracking.ConsignmentDate = CommonUtility.ToDateTime(objIDataReader["ConsignmentDate"]);
                    tracking.StatusUpdatedDate = CommonUtility.ToDateTime(objIDataReader["StatusUpdatedDate"]);
                    tracking.StatusUpdatedTime = CommonUtility.ToDateTime(objIDataReader["StatusUpdatedTime"]);
                    tracking.ImagePath = CommonUtility.ToString(objIDataReader["ImagePath"]);
                    tracking.ReceivedImage = CommonUtility.ToString(objIDataReader["ReceivedImage"]);
                    tracking.ReferenceData = CommonUtility.ToString(objIDataReader["ReferenceData"]);
                    tracking.StatusDescription = CommonUtility.ToString(objIDataReader["StatusDescription"]);
                    tracking.StatusIdentifier = CommonUtility.ToString(objIDataReader["StatusIdentifier"]);
                    Track.Add(tracking);
                }
            }
            return Track; 
    }
        /// <SUMMARY>
        /// Checks if consignment is loaded, and returns the status with data.
        /// </SUMMARY>
        /// <param name="consignment"></param>
        public IEnumerable<Tracking> GetItemDeliveryLoadDetail(Consignment consignment)
        {
            DBInitialize("spTrackDeliveryLoadByConNote");
            DatabaseName.AddInParameter(DBBaseCommand, "@ConNoteNumber", DbType.Int32, consignment.ConsignmentNumber);
            List<Tracking> Track = new List<Tracking>();
            Tracking tracking = new Tracking();
            using (IDataReader objIDataReader = DatabaseName.ExecuteReader(DBBaseCommand))
            {
                while (objIDataReader.Read())
                {
                    tracking = new Tracking();
                    tracking.ConsignmentNumber = CommonUtility.ToString(objIDataReader["ConsignmentNumber"]);
                    tracking.ConsignmentDate = CommonUtility.ToDateTime(objIDataReader["ConsignmentDate"]);
                    tracking.StatusUpdatedDate = CommonUtility.ToDateTime(objIDataReader["StatusUpdatedDate"]);
                    tracking.StatusUpdatedTime = CommonUtility.ToDateTime(objIDataReader["StatusUpdatedTime"]);
                    tracking.ImagePath = CommonUtility.ToString(objIDataReader["ImagePath"]);
                    tracking.ReceivedImage = CommonUtility.ToString(objIDataReader["ReceivedImage"]);
                    tracking.ReferenceData = CommonUtility.ToString(objIDataReader["ReferenceData"]);
                    tracking.StatusDescription = CommonUtility.ToString(objIDataReader["StatusDescription"]);
                    tracking.StatusIdentifier = CommonUtility.ToString(objIDataReader["StatusIdentifier"]);
                    Track.Add(tracking);
                }
            }
            return Track;
        }

        /// <SUMMARY>
        /// Checks if consignment is loaded for linehaul, and returns the status with data.
        /// </SUMMARY>
        /// <param name="consignment"></param>
        public IEnumerable<Tracking> GetLinehaulArriveDetail(Consignment consignment)
        {
            DBInitialize("spTrackTransitArriveConNote");
            DatabaseName.AddInParameter(DBBaseCommand, "@ConNoteNumber", DbType.Int32, consignment.ConsignmentNumber);
            List<Tracking> Track = new List<Tracking>();
            Tracking tracking = new Tracking();
            using (IDataReader objIDataReader = DatabaseName.ExecuteReader(DBBaseCommand))
            {
                while (objIDataReader.Read())
                {
                    tracking = new Tracking();
                    tracking.ConsignmentNumber = CommonUtility.ToString(objIDataReader["ConsignmentNumber"]);
                    tracking.ConsignmentDate = CommonUtility.ToDateTime(objIDataReader["ConsignmentDate"]);
                    tracking.StatusUpdatedDate = CommonUtility.ToDateTime(objIDataReader["StatusUpdatedDate"]);
                    tracking.StatusUpdatedTime = CommonUtility.ToDateTime(objIDataReader["StatusUpdatedTime"]);
                    tracking.ImagePath = CommonUtility.ToString(objIDataReader["ImagePath"]);
                    tracking.ReceivedImage = CommonUtility.ToString(objIDataReader["ReceivedImage"]);
                    tracking.ReferenceData = CommonUtility.ToString(objIDataReader["ReferenceData"]);
                    tracking.StatusDescription = CommonUtility.ToString(objIDataReader["StatusDescription"]);
                    tracking.StatusIdentifier = CommonUtility.ToString(objIDataReader["StatusIdentifier"]);
                    Track.Add(tracking);
                }
            }
            return Track;
        }

        /// <SUMMARY>
        /// Checks if consignment is loaded and departed for Linehaul, and returns the status with data.
        /// </SUMMARY>
        /// <param name="consignment"></param>
        public IEnumerable<Tracking> GetLinehaulDepartDetail(Consignment consignment)
        {
            
                DBInitialize("spTrackTransitDepartConNote");
            DatabaseName.AddInParameter(DBBaseCommand, "@ConNoteNumber", DbType.Int32, consignment.ConsignmentNumber);
            List<Tracking> Track = new List<Tracking>();
            Tracking tracking = new Tracking();
            using (IDataReader objIDataReader = DatabaseName.ExecuteReader(DBBaseCommand))
            {
                while (objIDataReader.Read())
                {
                    tracking = new Tracking();
                    tracking.ConsignmentNumber = CommonUtility.ToString(objIDataReader["ConsignmentNumber"]);
                    tracking.ConsignmentDate = CommonUtility.ToDateTime(objIDataReader["ConsignmentDate"]);
                    tracking.StatusUpdatedDate = CommonUtility.ToDateTime(objIDataReader["StatusUpdatedDate"]);
                    tracking.StatusUpdatedTime = CommonUtility.ToDateTime(objIDataReader["StatusUpdatedTime"]);
                    tracking.ImagePath = CommonUtility.ToString(objIDataReader["ImagePath"]);
                    tracking.ReceivedImage = CommonUtility.ToString(objIDataReader["ReceivedImage"]);
                    tracking.ReferenceData = CommonUtility.ToString(objIDataReader["ReferenceData"]);
                    tracking.StatusDescription = CommonUtility.ToString(objIDataReader["StatusDescription"]);
                    tracking.StatusIdentifier = CommonUtility.ToString(objIDataReader["StatusIdentifier"]);
                    Track.Add(tracking);
                }
            }
            return Track;
        }

        public IEnumerable<Tracking> GetOnforwardDetail(Consignment consignment)
        {
            DBInitialize("spTrackTransitByConNote");
            DatabaseName.AddInParameter(DBBaseCommand, "@ConNoteNumber", DbType.Int32, consignment.ConsignmentNumber);
            List<Tracking> Track = new List<Tracking>();
            Tracking tracking = new Tracking();
            using (IDataReader objIDataReader = DatabaseName.ExecuteReader(DBBaseCommand))
            {
                while (objIDataReader.Read())
                {
                    tracking = new Tracking();
                    tracking.ConsignmentNumber = CommonUtility.ToString(objIDataReader["ConsignmentNumber"]);
                    tracking.ConsignmentDate = CommonUtility.ToDateTime(objIDataReader["ConsignmentDate"]);
                    tracking.StatusUpdatedDate = CommonUtility.ToDateTime(objIDataReader["StatusUpdatedDate"]);
                    tracking.StatusUpdatedTime = CommonUtility.ToDateTime(objIDataReader["StatusUpdatedTime"]);
                    tracking.ImagePath = CommonUtility.ToString(objIDataReader["ImagePath"]);
                    tracking.ReceivedImage = CommonUtility.ToString(objIDataReader["ReceivedImage"]);
                    tracking.ReferenceData = CommonUtility.ToString(objIDataReader["ReferenceData"]);
                    tracking.StatusDescription = CommonUtility.ToString(objIDataReader["StatusDescription"]);
                    tracking.StatusIdentifier = CommonUtility.ToString(objIDataReader["StatusIdentifier"]);
                    Track.Add(tracking);
                }
            }
            return Track;            
        }

        /// <SUMMARY>
        /// Checks if Pickup is completed/created for consignment, and returns the status with data.
        /// </SUMMARY>
        /// <param name="consignment"></param>
        public IEnumerable<Tracking> GetPickupDetail(Consignment consignment)
        {
            DBInitialize("spTrackPickupByConNote");
            DatabaseName.AddInParameter(DBBaseCommand, "@ConNoteNumber", DbType.Int32, consignment.ConsignmentNumber);
            List<Tracking> Track = new List<Tracking>();
            Tracking tracking = new Tracking();
            using (IDataReader objIDataReader = DatabaseName.ExecuteReader(DBBaseCommand))
            {
                while (objIDataReader.Read())
                {
                    tracking = new Tracking();
                    tracking.ConsignmentNumber = CommonUtility.ToString(objIDataReader["ConsignmentNumber"]);
                    tracking.ConsignmentDate = CommonUtility.ToDateTime(objIDataReader["ConsignmentDate"]);
                    tracking.StatusUpdatedDate = CommonUtility.ToDateTime(objIDataReader["StatusUpdatedDate"]);
                    tracking.StatusUpdatedTime = CommonUtility.ToDateTime(objIDataReader["StatusUpdatedTime"]);
                    tracking.ImagePath = CommonUtility.ToString(objIDataReader["ImagePath"]);
                    tracking.ReceivedImage = CommonUtility.ToString(objIDataReader["ReceivedImage"]);
                    tracking.ReferenceData = CommonUtility.ToString(objIDataReader["ReferenceData"]);
                    tracking.StatusDescription = CommonUtility.ToString(objIDataReader["StatusDescription"]);
                    tracking.StatusIdentifier = CommonUtility.ToString(objIDataReader["StatusIdentifier"]);
                    Track.Add(tracking);
                }
            }
            return Track;            
        }
    }
}
