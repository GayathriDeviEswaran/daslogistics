﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Web;

namespace Logistics.ExceptionHandling
{
    public class CommonException
    {

        private static String ErrorlineNo, Errormsg, extype, exurl, ErrorLocation;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /*
         * 
         Description : write a application error in txt file without using log4net.dll
         */
        public static void SendErrorToText(Exception ex, String hostIp)
        {
            var line = Environment.NewLine + Environment.NewLine;

            ErrorlineNo = ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7);
            Errormsg = ex.GetType().Name.ToString();
            extype = ex.GetType().ToString();
            exurl = HttpContext.Current.Request.Url.ToString();
            ErrorLocation = ex.Message.ToString();
             
            try
            {
                string filepath = HttpContext.Current.Server.MapPath("~/ExceptionDetailsFile/");  //Text File Path

                if (!Directory.Exists(filepath))
                {
                    Directory.CreateDirectory(filepath);

                }
                filepath = filepath + DateTime.Today.ToString("dd-MM-yy") + ".txt";   //Text File Name
                if (!File.Exists(filepath))
                {


                    File.Create(filepath).Dispose();

                }
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    string error = "Log Written Date:" + " " + DateTime.Now.ToString() + line + "Error Line No :" + " " + ErrorlineNo + line + "Error Message:" + " " + Errormsg + line + "Exception Type:" + " " + extype + line + "Error Location :" + " " + ErrorLocation + line + " Error Page Url:" + " " + exurl + line + "User Host IP:" + " " + hostIp + line;
                    sw.WriteLine("-----------Exception Details on " + " " + DateTime.Now.ToString() + "-----------------");
                    sw.WriteLine("-------------------------------------------------------------------------------------");
                    sw.WriteLine(line);
                    sw.WriteLine(error);
                    sw.WriteLine("--------------------------------*End*------------------------------------------");
                    sw.WriteLine(line);
                    sw.Flush();
                    sw.Close();

                }

            }
            catch (Exception e)
            {
                e.ToString();

            }
        }

        /*
         * 
         Description : write a application error in txt file(.Err extenstion) using log4net.dll
         */
        public static void Writelog(Exception ex, String hostIp)
        {

            ErrorlineNo = ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7);
            Errormsg = ex.GetType().Name.ToString();
            extype = ex.GetType().ToString();
            exurl = HttpContext.Current.Request.Url.ToString();
            ErrorLocation = ex.Message.ToString();

            log.Error(ex.ToString());
            log.Error("ErrorlineNo :" + ErrorlineNo);
            log.Error("Errormsg :" + Errormsg);
            log.Error("extype :" + extype);
            log.Error("exurl :" + exurl);
            log.Error("ErrorLocation :" + ErrorLocation);
            log.Error("HostIp :" + hostIp);
        }
    }
}
