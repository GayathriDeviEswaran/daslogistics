﻿using Logistics.BusinessAccessInterface;
using Logistics.DataAccess;
using Logistics.DataAccessInterface;
using Logistics.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistics.BusinessAccess
{
    class Login : ILogin
    {
        ILoginDA _Obj = new LoginDA();
        public ResponseLogin ValidateLogin(string username, string password)
        {
            return _Obj.ValidateLogin(username, password);
        }        
    }
}
