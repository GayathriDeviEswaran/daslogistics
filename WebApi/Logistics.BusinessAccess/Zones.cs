﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logistics.BusinessAccessInterface;
using Logistics.DataAccess;
using Logistics.DataAccessInterface;
using Logistics.Entity;
namespace Logistics.BusinessAccess
{
    public class Zones: IZones
    {
        IZonesDA _obj = new ZonesDA();
        public IEnumerable<Zone> AddZones(Logistics.Entity.Zone data)
        {
            return _obj.AddZones(data);
        }
    }
}
