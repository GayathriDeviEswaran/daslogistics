﻿using Logistics.BusinessAccessInterface;
using Logistics.DataAccess;
using Logistics.DataAccessInterface;
using Logistics.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistics.BusinessAccess
{
     public class Notification: INotification
    {
        INotificationDA _obj = new NotificationDA();
         public IEnumerable<NotificationTemplate> GetNotificationByProcessID(Logistics.Entity.Notification ProcessID)
        {
            return _obj.GetNotificationByProcessID(ProcessID);
        }
        public IEnumerable<NotificationTemplate> GetNotificationByID(Logistics.Entity.Notification ID)
        {
            return _obj.GetNotificationByID(ID);
        }
        public IEnumerable<NotificationTemplate> AddNotification(Logistics.Entity.NotificationTemplate data)
        {
            return _obj.AddNotification(data);
        }
    }
}
