﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logistics.BusinessAccessInterface;
using Logistics.DataAccess;
using Logistics.DataAccessInterface;
using Logistics.Entity;
namespace Logistics.BusinessAccess
{
    public class Activitylogs: IActivitylogs
    {
        IActivitylogsDA _obj = new ActivitylogsDA();
        public IEnumerable< Logistics.Entity.Activitylogs> AddActivityLogs(Logistics.Entity.Activitylogs data)
        {
            return _obj.AddActivityLogs(data);
        }
    }
}
