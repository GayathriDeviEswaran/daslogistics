﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logistics.Entity;
using Logistics.BusinessAccessInterface;
using Logistics.DataAccess;
using Logistics.DataAccessInterface;

namespace Logistics.BusinessAccess
{
    public class AccountTerms: IAccountTerms
    {
        IAccountTermsDA _obj = new AccountTermsDA();
        public IEnumerable<Logistics.Entity.AccountTerms> GetAccountTerms(Logistics.Entity.AccountTerms data)
        {
            return _obj.GetAccountTerms(data);
        }
    }
}
