﻿using Logistics.BusinessAccessInterface;
using Logistics.DataAccess;
using Logistics.DataAccessInterface;
using Logistics.Entity;
using System;
using System.Collections.Generic;

namespace Logistics.BusinessAccess
{
    public class ConsignmentTracker : IConsignmentTracker
    {
        IConsignmentTrackerDA _Obj= new ConsignmentTrackerDA();
        public IEnumerable<Tracking> GetInvoiceByConNote(Consignment consignment)
        {
            return _Obj.GetInvoiceByConNote(consignment);
        }
        public IEnumerable<Tracking> GetPickupDetail(Consignment consignment)
        {
            return _Obj.GetPickupDetail(consignment);
        }
        public IEnumerable<Tracking> GetLinehaulDepartDetail(Consignment consignment)
        {
            return _Obj.GetLinehaulDepartDetail(consignment);
        }
        public IEnumerable<Tracking> GetLinehaulArriveDetail(Consignment consignment)
        {
            return _Obj.GetLinehaulArriveDetail(consignment);
        }
        public IEnumerable<Tracking> GetOnforwardDetail(Consignment consignment)
        {
            return _Obj.GetOnforwardDetail(consignment);
        }
        public IEnumerable<Tracking> GetItemDeliveryLoadDetail(Consignment consignment)
        {
            return _Obj.GetItemDeliveryLoadDetail(consignment);
        }
        public IEnumerable<Tracking> GetItemDeliveredDetail(Consignment consignment)
        {
            return _Obj.GetItemDeliveredDetail(consignment);
        }
        public IEnumerable<Tracking> GetConsignmentTracker(Consignment consignment)
        {
            return _Obj.GetConsignmentTracker(consignment);
        }
    }
}
