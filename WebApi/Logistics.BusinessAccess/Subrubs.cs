﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logistics.Entity;
using Logistics.BusinessAccessInterface;
using Logistics.DataAccess;
using Logistics.DataAccessInterface;


namespace Logistics.BusinessAccess
{
    public class Subrubs: ISubrubs
    {
        ISubrupsDA _obj = new SubrubsDA();
        public IEnumerable<SubrupsEntity> GetSubrupsDetailsbyID(Entity.SubrupsID SubrupsID)
        {
            return _obj.GetSubrupsDetailsbyID(SubrupsID);
        }
    }
}
