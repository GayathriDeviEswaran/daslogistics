﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logistics.BusinessAccessInterface;
using Logistics.DataAccess;
using Logistics.DataAccessInterface;
using Logistics.Entity;

namespace Logistics.BusinessAccess
{
    public class MailNotification: IMailNotification
    {
        IMailNotificationDA _obj = new MailNotificationDA();
        public IEnumerable<MailNotificationTemplate> GetMailNotificationByProcessID(Logistics.Entity.MailNotification ProcessID)
        {
            return _obj.GetMailNotificationByProcessID(ProcessID);
        }
        public IEnumerable<MailNotificationTemplate> GetMailNotificationByID(Logistics.Entity.MailNotification ID)
        {
            return _obj.GetMailNotificationByID(ID);
        }
        public IEnumerable<MailNotificationTemplate> AddMailNotification(Logistics.Entity.MailNotificationTemplate data)
        {
            return _obj.AddMailNotification(data);
        }

    }
}
