﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistics.Entity
{

    [Serializable]
    public class ErrorContext
    {
        public string ErrorNo { get; set; }
        public string ErrorDescription { get; set; }
    }
    [Serializable]
    public class ResponseTrackerContext
    {
        public List<ErrorContext> Errors { get; set; }
        public bool Status { get; set; }
        public List<Tracking> TrackingDetails { get; set; }
    }
    [Serializable]
    public class ResponseNotificiationContext
    {
        public List<ErrorContext> Errors { get; set; }
        public bool Status { get; set; }
        public List<NotificationTemplate> NotificationDetails { get; set; }
    }
    [Serializable]
    public class ResponseMailNotificiationContext
    {
        public List<ErrorContext> Errors { get; set; }
        public bool RStatus { get; set; }
        public List<MailNotificationTemplate> MailNotificationDetails { get; set; }
    }
    [Serializable]
    public class AppSettings
    {
        public string KeyName { get; set; }
        public string KeyValue { get; set; }
    }

    public class Login
    {
        public string LoginID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    public sealed class AccessToken
    {
        public string Token { get; }
        public DateTime ExpiresIn { get; }

        public AccessToken(string token, DateTime expiresIn)
        {
            Token = token;
            ExpiresIn = expiresIn;
        }
    }
}
