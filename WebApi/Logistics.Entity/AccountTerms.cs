﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistics.Entity
{
   public class AccountTerms
    {
        public int AccountTermsID { get; set; }
        public String Description { get; set; }
        public bool Active { get; set; }
    }
}
