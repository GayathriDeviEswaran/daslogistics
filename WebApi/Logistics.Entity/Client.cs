﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Logistics.Entity
{
    [Table("t_vl_ClientMaster")]
    public class ClientEntity
    {
        [Key]
        public int ClientKeyId { get; set; }

        [Required]
        [StringLength(500)]
        public string ClientID { get; set; }

        [Required]
        [StringLength(500)]
        public string ClientSecret { get; set; }

        [Required]
        [StringLength(100)]
        public string ClientName { get; set; }

        public bool Active { get; set; }

        public int RefreshTokenLifeTime { get; set; }

        [Required]
        [StringLength(500)]
        public string AllowedOrigin { get; set; }

    }
}
