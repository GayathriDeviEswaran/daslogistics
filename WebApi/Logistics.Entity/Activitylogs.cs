﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistics.Entity
{
    public class Activitylogs
    {
        public int UserID { get; set; }
        public String FunctionPerformed { get; set; }
        public string URL { get; set; }
        public string IPAddress { get; set; }

    }
}
