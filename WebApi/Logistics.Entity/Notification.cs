﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistics.Entity
{
   public class Notification
    {
        public int ProcessID { get; set; }   
    }
    public class NotificationTemplate
    { 
        public int NotificationTemplateID { get; set; }
        public string Notification_Template_Name { get; set; }
        public string Notification_Message { get; set; }
        public int ProcessID { get; set; }
        public int Status { get; set; }
        public int Notification_TypeID { get; set; }

        public int IsActive { get; set; }
        public int UserID { get; set; }

    }
}
