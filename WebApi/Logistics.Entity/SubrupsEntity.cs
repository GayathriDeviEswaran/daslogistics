﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistics.Entity
{
    public class SubrupsEntity
    {
        public int id { get; set; }
        public string Subrub { get; set; }
        public string State { get; set; }
        public int Postcode { get; set; }
        public String DepotsAndRun { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }

        public int Status { get; set; }

    }

    public class SubrupsID { 
        public int ID { get; set; }
    }
}
