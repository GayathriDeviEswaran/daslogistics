﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistics.Entity
{    
    public class RequestTokenValidate
    {
        public string token { get; set; }
    }
    public class RequestLogin
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
    public class ResponseLogin
    {
        public String SessionToken { get; set; }
        public string UserName { get; set; }
        public RoleMaster Role { get; set; }
        public List<MenuMaster> MenuMasters { get; set; }
        public Error Errors { get; set; }
    }
    public class LoginVal
    {
        public int State { get; set; }
    }
    public class Error
    {
        public bool status { get; set; }
        public string ErrorMessage { get; set; }
    }
    public class LoginModal
    {
        public int ID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public int status { get; set; }
    }
    public class MenuMaster
    {
        public int ID { get; set; }
        public int? ParentID { get; set; }
        public string URL { get; set; }
        public string DisplayName { get; set; }
        public string MenuIcon { get; set; }
        public int DisplayOrder { get; set; }
        public int status { get; set; }
    }
    public class MenuRoleMapping
    {
        public int ID { get; set; }
        public int MenuID { get; set; }
        public MenuMaster MenuMaster { get; set; }
        public int RoleID { get; set; }
        public RoleMaster RoleMaster { get; set; }
        public int status { get; set; }
    }
    public class RoleMaster
    {
        public int ID { get; set; }
        public string RoleName { get; set; }
        public int status { get; set; }
    }
    public class TokenLog
    {
        public int ID { get; set; }
        public string UserName { get; set; }
        public string token { get; set; }
        public int status { get; set; }
    }
}
