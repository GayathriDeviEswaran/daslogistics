﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistics.Entity
{
    public class MailNotification
    {
        public int ProcessID { get; set; }
    }
    public class MailNotificationTemplate
    {

        public int MailNotificationTemplateID { get; set; }
        public string MailNotification_Template_Name { get; set; }
        public string MailNotification_Message { get; set; }
        public int ProcessID { get; set; }
        public int RStatus { get; set; }
        public int MailNotification_TypeID { get; set; }
        public int IsActive { get; set; }
        public int UserID { get; set; }

    }
}
