﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistics.Entity
{
    public class Consignment
    {
        public string ConsignmentNumber { get; set; }
    }

    public class Tracking
    {
        public string ConsignmentNumber { get; set; }
        public DateTime? ConsignmentDate { get; set; }
        public string StatusIdentifier { get; set; }
        public DateTime? StatusUpdatedDate { get; set; }
        public DateTime? StatusUpdatedTime { get; set; }
        public string ReferenceData { get; set; }
        public string StatusDescription { get; set; }
        public string ImagePath { get; set; }
        public string ReceivedImage { get; set; }
    }

}
