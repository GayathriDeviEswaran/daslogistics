﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistics.Entity
{
   public class Zone
    {
        public String Zone_no { get; set; }   
        public String Zone_Name { get; set; }
        public String Zone_Prefix { get; set; }
        public int Status { get; set; }

        public int UserID { get; set; }

    }
}
