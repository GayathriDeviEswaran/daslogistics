﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Web.Http.Description;
using System.Net.Http;
using System.Web.Mvc;
using Logistics.BusinessAccessInterface;
using Logistics.Entity;
using Logistics.ExceptionHandling;


namespace Logistics.Controllers
{
    public class ActivitylogsController : System.Web.Http.ApiController
    {
        private readonly IActivitylogs _Activitylog;

        #region "AddActivity"
        /// <summary>
        /// This service is used to insert activity log based on user.
        /// </summary>
        /// <param name="data"><see cref="ApiDescription"/></param>
        /// <returns>Activity Logs details </returns>
        [HttpPost]
        [Route("AddActivitylogs")]
        [ResponseType(typeof(NotificationTemplate))]
        public HttpResponseMessage AddActivitylogs(Activitylogs data)
        { 
            try
            {
                var _returnData = _Activitylog.AddActivityLogs(data);
                HttpResponseMessage httpResponse = Request.CreateResponse(_returnData);
                return httpResponse;
            }
            catch (Exception ex)
            {
                CommonException.SendErrorToText(ex, "");
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);

            }

        }

        #endregion
    }
}