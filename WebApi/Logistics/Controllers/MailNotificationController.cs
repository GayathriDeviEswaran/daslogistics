﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Web.Http.Description;
using System.Net.Http;
using Logistics.BusinessAccessInterface;
using Logistics.Entity;

namespace Logistics.Controllers
{
    public class MailNotificationController : System.Web.Http.ApiController
    {
        // GET: MailNotification
        private readonly IMailNotification _MailNotificationService;
        public MailNotificationController(IMailNotification notificationService)
        {
            _MailNotificationService = notificationService;
        }
        #region "GetCreatedMailNotificationByProcessID"
        /// <summary>
        /// This service is used to retrun the MailNotification details based on ProcessID.
        /// </summary>
        /// <param name="ProessID"><see cref="ApiDescription"/></param>
        /// <returns>MailNotification details </returns>
        [HttpGet]
        [Route("GetCreatedMailNotificationByProcessID")]
        [ResponseType(typeof(MailNotificationTemplate))]
        public HttpResponseMessage GetMailNotificationByProcessID(MailNotification ProcessID)
        {
            var _returnData = _MailNotificationService.GetMailNotificationByProcessID(ProcessID);
            HttpResponseMessage httpResponse = MailNotificationResponse(_returnData, ProcessID);
            return httpResponse;
        }

        #endregion
        #region "GetCreatedMailNotificationByID"
        /// <summary>
        /// This service is used to retrun the MailNotification details based on ProcessID.
        /// </summary>
        /// <param name="ID"><see cref="ApiDescription"/></param>
        /// <returns>Notification details </returns>
        [HttpGet]
        [Route("GetCreatedMailNotificationByID")]
        [ResponseType(typeof(MailNotificationTemplate))]
        public HttpResponseMessage GetMailNotificationByID(MailNotification ID)
        {
            var _returnData = _MailNotificationService.GetMailNotificationByID(ID);
            HttpResponseMessage httpResponse = MailNotificationResponse(_returnData, ID);
            return httpResponse;
        }

        #endregion
        #region "AddMailNotification"
        /// <summary>
        /// This service is used to insert and update the Mailnotification.
        /// </summary>
        /// <param name="data"><see cref="ApiDescription"/></param>
        /// <returns>Notification details </returns>
        [HttpGet]
        [Route("AddMailNotification")]
        [ResponseType(typeof(MailNotificationTemplate))]
        public HttpResponseMessage AddMailNotification(MailNotificationTemplate data)
        {
            var _returnData = _MailNotificationService.AddMailNotification(data);
            HttpResponseMessage httpResponse = Request.CreateResponse(_returnData); ;
            //ResponseMailNotificiationContext apiResponse;
            //List<MailNotificationTemplate> entities = new List<MailNotificationTemplate>();
            //foreach (var x in _returnData)
            //{
            //    entities.Add(x); entities.Add(x);
            //}

            //apiResponse = new ResponseMailNotificiationContext()
            //{
            //    RStatus = true,
            //    MailNotificationDetails = entities
            //};
            //httpResponse = Request.CreateResponse(_returnData);
            return httpResponse;
        }

        #endregion
        #region "MailNotificationResponse"
        /// <summary>
        /// This service is used display the notification details
        /// </summary>
        /// <param name="ProessID"><see cref="ApiDescription"/></param>
        /// <param name="trackEntity"><see cref="ApiDescription"/></param>
        /// <returns>tracking information along with the status.</returns>
        private HttpResponseMessage MailNotificationResponse(IEnumerable<MailNotificationTemplate> trackEntity, MailNotification ProcessID)
        {
            HttpResponseMessage httpResponse;
            ResponseMailNotificiationContext apiResponse;
            List<ErrorContext> ecn = new List<ErrorContext>();
            if (trackEntity == null)
            {
                string message = string.Format("No Notification with ID = {0} found!", ProcessID.ProcessID);
                ErrorContext ec = new ErrorContext();
                ec.ErrorNo = "";
                ec.ErrorDescription = message;
                ecn.Add(ec);
                apiResponse = new ResponseMailNotificiationContext()
                {
                    Errors = ecn,
                    RStatus = false
                };
                httpResponse = Request.CreateResponse(apiResponse);
                return httpResponse;
                //return Request.CreateErrorResponse(HttpStatusCode.NotFound, message);
            }
            List<MailNotificationTemplate> entities = new List<MailNotificationTemplate>();// = trackEntity.ToList<Tracking>();
            foreach (var x in trackEntity)
            {
                //if (!(string.IsNullOrEmpty(x.ImagePath)))
                //{
                //    x.ReceivedImage =
                //        System.IO.File.Exists(ConfigurationManager.AppSettings["PODImage"] + x.ImagePath.Replace(@"/", @"\")) ?
                //        Convert.ToBase64String(System.IO.File.ReadAllBytes
                //      (ConfigurationManager.AppSettings["PODImage"] + x.ImagePath.Replace(@"/", @"\"))) : "";
                //}
                entities.Add(x);
            }
            apiResponse = new ResponseMailNotificiationContext()
            {
                RStatus = true,
                MailNotificationDetails = entities
            };
            httpResponse = Request.CreateResponse(apiResponse);
            return httpResponse;
        }
        #endregion


    }
}