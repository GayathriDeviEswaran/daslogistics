﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Web.Mvc;
using Logistics.BusinessAccessInterface;
using Logistics.Entity;
using Logistics.ExceptionHandling;
using System.Net.Http;
using System.Web.Http.Description;

namespace Logistics.Controllers
{
    public class ZonesController : System.Web.Http.ApiController
    {
        private readonly IZones _zoneslog;

        #region "AddZones"
        /// <summary>
        /// This service is used to insert activity log based on user.
        /// </summary>
        /// <param name="data"><see cref="ApiDescription"/></param>
        /// <returns>Activity Logs details </returns>
        [HttpPost]
        [Route("AddZones")]
        [ResponseType(typeof(Zone))]
        public HttpResponseMessage AddActivitylogs(Zone data)
        {
            try
            {
                var _returnData = _zoneslog.AddZones(data);
                HttpResponseMessage httpResponse = Request.CreateResponse(_returnData);
                return httpResponse;
            }
            catch (Exception ex)
            {
                CommonException.SendErrorToText(ex, "");
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);

            }

        }

        #endregion
    }
}