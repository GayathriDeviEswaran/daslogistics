﻿using Logistics.BusinessAccessInterface;
using Logistics.Entity;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http.Description;
using System.Web.Mvc;

namespace Logistics.Controllers
{
    public class AutheticationController : System.Web.Http.ApiController
    {
        private readonly ILogin __Login;

        public AutheticationController(ILogin login)
        {
            __Login = login;
        }

        #region "ValidateLogin"
        /// <summary>
        /// This service allows the user to validate his logging creadentials.
        /// </summary>
        /// <param name="username"><see cref="ApiDescription"/></param>
        /// <param name="password"><see cref="ApiDescription"/></param>
        /// <returns>menu details and user details with session token.</returns>
        [HttpGet]
        [Route("ValidateLogin")]
        [ResponseType(typeof(ResponseLogin))]
        public HttpResponseMessage ValidateLogin(string username,string password)
        {
            var _returnData = __Login.ValidateLogin(username,password);
            HttpResponseMessage httpResponse = Request.CreateResponse(_returnData);
            return httpResponse;
        }
        #endregion
    }
}