﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Web.Http.Description;
using System.Net.Http;
using System.Web.Mvc;
using Logistics.BusinessAccessInterface;
using Logistics.Entity;
using Logistics.ExceptionHandling;

namespace Logistics.Controllers
{
    public class NotificationController : System.Web.Http.ApiController
    {
        private readonly INotification _NotificationService;
        public NotificationController(INotification notificationService)
        {
            _NotificationService = notificationService;
        }
        #region "GetCreatedNotificationByProcessID"
        /// <summary>
        /// This service is used to retrun the Notification details based on ProcessID.
        /// </summary>
        /// <param name="ProcessID"><see cref="ApiDescription"/></param>
        /// <returns>Notification details </returns>
        [HttpGet]
        [Route("GetCreatedNotificationByProcessID")]
        [ResponseType(typeof(NotificationTemplate))]
        public HttpResponseMessage GetNotificationByProcessID(Notification ProcessID)
        {
            var _returnData = _NotificationService.GetNotificationByProcessID(ProcessID);
            HttpResponseMessage httpResponse = NotificationResponse(_returnData, ProcessID);
            return httpResponse;
        }

        #endregion
        #region "GetCreatedNotificationByID"
        /// <summary>
        /// This service is used to retrun the Notification details based on ProcessID.
        /// </summary>
        /// <param name="ID"><see cref="ApiDescription"/></param>
        /// <returns>Notification details </returns>
        [HttpGet]
        [Route("GetCreatedNotificationByID")]
        [ResponseType(typeof(NotificationTemplate))]
        public HttpResponseMessage GetNotificationByID(Notification ID)
        {
            var _returnData = _NotificationService.GetNotificationByID(ID);
            HttpResponseMessage httpResponse = NotificationResponse(_returnData, ID);
            return httpResponse;
        }

        #endregion
        #region "AddNotification"
        /// <summary>
        /// This service is used to insert and update the notification.
        /// </summary>
        /// <param name="data"><see cref="ApiDescription"/></param>
        /// <returns>Notification details </returns>
        [HttpPost]
        [Route("AddNotification")]
        [ResponseType(typeof(NotificationTemplate))]
        public HttpResponseMessage AddNotification(NotificationTemplate data)
        {
            HttpResponseMessage httpResponse;
            try
            {
                var _returnData = _NotificationService.AddNotification(data);
               
                ResponseNotificiationContext apiResponse;
                List<NotificationTemplate> entities = new List<NotificationTemplate>();
                foreach (var x in _returnData)
                {
                    entities.Add(x); entities.Add(x);
                }

                apiResponse = new ResponseNotificiationContext()
                {
                    Status = true,
                    NotificationDetails = entities
                };
                httpResponse = Request.CreateResponse(apiResponse);
                return httpResponse;
            }
            catch  (Exception ex) {
               CommonException.SendErrorToText(ex,  "");
                return  Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);

            }
            
        }

        #endregion
        #region "NotificationResponse"
        /// <summary>
        /// This service is used display the notification details
        /// </summary>
        /// <param name="consignmentNo"><see cref="ApiDescription"/></param>
        /// <param name="trackEntity"><see cref="ApiDescription"/></param>
        /// <returns>tracking information along with the status.</returns>
        private HttpResponseMessage NotificationResponse(IEnumerable<NotificationTemplate> trackEntity, Notification ProcessID)
        {
            HttpResponseMessage httpResponse;
            ResponseNotificiationContext apiResponse;
            List<ErrorContext> ecn = new List<ErrorContext>();
            if (trackEntity == null)
            {
                string message = string.Format("No Notification with ID = {0} found!", ProcessID.ProcessID);
                ErrorContext ec = new ErrorContext();
                ec.ErrorNo = "";
                ec.ErrorDescription = message;
                ecn.Add(ec);
                apiResponse = new ResponseNotificiationContext()
                {
                    Errors = ecn,
                    Status = false
                };
                httpResponse = Request.CreateResponse(apiResponse);
                return httpResponse;
                //return Request.CreateErrorResponse(HttpStatusCode.NotFound, message);
            }
            List<NotificationTemplate> entities = new List<NotificationTemplate>();// = trackEntity.ToList<Tracking>();
            foreach (var x in trackEntity)
            {
                //if (!(string.IsNullOrEmpty(x.ImagePath)))
                //{
                //    x.ReceivedImage =
                //        System.IO.File.Exists(ConfigurationManager.AppSettings["PODImage"] + x.ImagePath.Replace(@"/", @"\")) ?
                //        Convert.ToBase64String(System.IO.File.ReadAllBytes
                //      (ConfigurationManager.AppSettings["PODImage"] + x.ImagePath.Replace(@"/", @"\"))) : "";
                //}
                entities.Add(x);
            }
            //entities.ForEach(x =>
            //{
            //    if (!(string.IsNullOrEmpty(x.ImagePath)))
            //    {
            //        x.ReceivedImage =
            //            File.Exists(ConfigurationManager.AppSettings["PODImage"] + x.ImagePath.Replace(@"/", @"\")) ?
            //            Convert.ToBase64String(System.IO.File.ReadAllBytes
            //          (ConfigurationManager.AppSettings["PODImage"] + x.ImagePath.Replace(@"/", @"\"))) : "";
            //    }
            //});
            //TrackResultEntity trackResultEntity = new TrackResultEntity()
            //{
            //    status_code = ServiceConstants.CodeSuccess,
            //    trackingEntities = entities
            //    //response = new ResponseEntity()
            //    //{
            //    //    status_code = ServiceConstants.CodeSuccess
            //    //}
            //};
            apiResponse = new ResponseNotificiationContext()
            {
                Status = true,
                NotificationDetails = entities
            };
            httpResponse = Request.CreateResponse(apiResponse);
            return httpResponse;
        }
        #endregion


    }
}