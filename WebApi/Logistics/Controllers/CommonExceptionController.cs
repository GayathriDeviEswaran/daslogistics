﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Logistics.BusinessAccessInterface;
using Logistics.Entity;
namespace Logistics.Controllers
{
    public class CommonExceptionController : System.Web.Http.ApiController
    {
        private readonly ICommonException _commonException;
        /*
         * 
         * 
         * Description : Adderrorlog : write a log file without using log4net dll
         */

        #region Errorlogwithoutlog4net
        /*
     * 
     * 
     * Description : Adderrorlog : write a log file WITHOUT using log4net dll
     */
        [HttpGet]
        public void AddErrorlog(CommonErrorException error) {
            _commonException.AddErrorlog(error);
        }
        #endregion

        #region ErrorlogUsinglog4net
        /*
        * 
        * 
        * Description : Writelog : write a log file using log4net dll
        */
        [HttpGet]
        public void Writelog(CommonErrorException error)
        {
            _commonException.AddErrorlog(error);
        }
        #endregion
    }
}