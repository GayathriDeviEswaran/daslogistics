﻿using Logistics.BusinessAccessInterface;
using Logistics.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Description;
using System.IO;
using System.Web.Mvc;

namespace Logistics.Controllers
{
    public class ConsignmentTrackController : System.Web.Http.ApiController
    {

        private readonly IConsignmentTracker __consignmentTrackService;

        public ConsignmentTrackController(IConsignmentTracker consignTrackService)
        {
            __consignmentTrackService = consignTrackService;
        }

        #region "GetCreatedConsigment"
        /// <summary>
        /// This service allows the client to track the progress of item by returning the consignment creation status for a single consignment.
        /// </summary>
        /// <param name="consignmentNo"><see cref="ApiDescription"/></param>
        /// <returns>tracking information along with the status.</returns>
        [HttpGet]
        [Route("GetCreatedConsigment")]
        [ResponseType(typeof(Tracking))]
        public HttpResponseMessage TrackInvoiceConsignment(Consignment consignmentNo)
        {
            var _returnData = __consignmentTrackService.GetInvoiceByConNote(consignmentNo);
            HttpResponseMessage httpResponse = ConsignmentResponse(_returnData, consignmentNo);
            return httpResponse;
        }

        #endregion
        #region "GetPickupConsignment"
        /// <summary>
        /// This service allows the client to track the progress of item by returning the Pickup status for a single passed consignment.
        /// </summary>
        /// <param name="consignmentNo"><see cref="ApiDescription"/></param>
        /// <returns>tracking information along with the status.</returns>
        [Route("GetPickupConsignment")]
        [HttpGet]
        [ResponseType(typeof(Tracking))]
        public HttpResponseMessage TrackPickupConsignments(Consignment consignmentNo)
        {
            var _returnData = __consignmentTrackService.GetPickupDetail(consignmentNo);
            HttpResponseMessage httpResponse = ConsignmentResponse(_returnData, consignmentNo);
            return httpResponse;
        }
        #endregion
        #region "GetLinehaulDepartConsigment"
        /// <summary>
        /// This service allows the client to track the progress of item by returning the status of Interstart Departures for a single passed consignment.
        /// </summary>
        /// <param name="consignmentNo"><see cref="ApiDescription"/></param>
        /// <returns>tracking information along with the status.</returns>
        [Route("GetLinehaulDepartConsigment")]
        [HttpGet]
        [ResponseType(typeof(Tracking))]
        public HttpResponseMessage TrackTransitDepartConsignments(Consignment consignmentNo)
        {
            var _returnData = __consignmentTrackService.GetLinehaulDepartDetail(consignmentNo);
            HttpResponseMessage httpResponse = ConsignmentResponse(_returnData, consignmentNo);
            return httpResponse;
        }
        #endregion
        #region "GetLinehaulArriveConsigment"
        /// <summary>
        /// This service allows the client to track the progress of item by returning the status of Interstate Arrivals for a single passed consignment.
        /// </summary>
        /// <param name="consignmentNo"><see cref="ApiDescription"/></param>
        /// <returns>tracking information along with the status.</returns>
        [Route("GetLinehaulArriveConsigment")]
        [HttpGet]
        [ResponseType(typeof(Tracking))]
        public HttpResponseMessage TrackTransitArriveConsignments(Consignment consignmentNo)
        {
            var _returnData = __consignmentTrackService.GetLinehaulArriveDetail(consignmentNo);
            HttpResponseMessage httpResponse = ConsignmentResponse(_returnData, consignmentNo);
            return httpResponse;
        }
        #endregion
        #region "GetOnforwardConsigment"
        /// <summary>
        /// This service allows the client to track the progress of item by returning the status of Onforwarders for a single passed consignment.
        /// </summary>
        /// <param name="consignmentNo"><see cref="ApiDescription"/></param>
        /// <returns>tracking information along with the status.</returns>
        [Route("GetOnforwardConsigment")]
        [HttpGet]
        [ResponseType(typeof(Tracking))]
        public HttpResponseMessage TrackOnForwardConsignments(Consignment consignmentNo)
        {
            var _returnData = __consignmentTrackService.GetOnforwardDetail(consignmentNo);
            HttpResponseMessage httpResponse = ConsignmentResponse(_returnData, consignmentNo);
            return httpResponse;
        }
        #endregion
        #region "GetLoadDeliveryConsigment"
        /// <summary>
        /// This service allows the client to track the progress of item by returning the status of Delivery Load information for a single passed consignment.
        /// </summary>
        /// <param name="consignmentNo"><see cref="ApiDescription"/></param>
        /// <returns>tracking information along with the status.</returns>
        [Route("GetLoadDeliveryConsigment")]
        [HttpGet]
        [ResponseType(typeof(Tracking))]
        public HttpResponseMessage TrackDeliveryLoadConsignments(Consignment consignmentNo)
        {
            var _returnData = __consignmentTrackService.GetItemDeliveryLoadDetail(consignmentNo);
            HttpResponseMessage httpResponse = ConsignmentResponse(_returnData, consignmentNo);
            return httpResponse;
        }
        #endregion
        #region "GetDeliveredConsigment"
        /// <summary>
        /// This service allows the client to track the progress of item by returning the Delivered Information along with signed scanned images for a single passed consignment.
        /// </summary>
        /// <param name="consignmentNo"><see cref="ApiDescription"/></param>
        /// <returns>tracking information along with the status.</returns>
        [Route("GetDeliveredConsigment")]
        [HttpGet]
        [ResponseType(typeof(Tracking))]
        public HttpResponseMessage TrackDeliveredDetail(Consignment consignmentNo)
        {
            var _returnData = __consignmentTrackService.GetItemDeliveredDetail(consignmentNo);
            HttpResponseMessage httpResponse = ConsignmentResponse(_returnData, consignmentNo);
            return httpResponse;
        }
        #endregion
        #region "GetConsigmentTracker"
        /// <summary>
        /// This service allows the client to track the progress of item by returning the consolidated delivery status for a single passed consignment.
        /// </summary>
        /// <param name="consignmentNo" type="Consignment"><see cref="ApiDescription"/></param>
        /// <returns>tracking information along with the status.</returns>
        [Route("GetConsigmentTracker")]
        [HttpGet]
        [ResponseType(typeof(Tracking))]
        public HttpResponseMessage TrackDetailedConsignment(Consignment consignmentNo)
        {
            var _returnData = __consignmentTrackService.GetConsignmentTracker(consignmentNo);
            HttpResponseMessage httpResponse = ConsignmentResponse(_returnData, consignmentNo);
            return httpResponse;
        }
        #endregion
        #region "ConsignmentResponse"
        /// <summary>
        /// This service allows the client to track the progress of item by returning the Pickup status for a single passed consignment.
        /// </summary>
        /// <param name="consignmentNo"><see cref="ApiDescription"/></param>
        /// <param name="trackEntity"><see cref="ApiDescription"/></param>
        /// <returns>tracking information along with the status.</returns>
        private HttpResponseMessage ConsignmentResponse(IEnumerable<Tracking> trackEntity, Consignment consignmentNo)
        {
            HttpResponseMessage httpResponse;
            ResponseTrackerContext apiResponse;
            List<ErrorContext> ecn = new List<ErrorContext>();
            if (trackEntity == null)
            {
                string message = string.Format("No Consignments with ID = {0} found!", consignmentNo.ConsignmentNumber);
                ErrorContext ec = new ErrorContext();
                ec.ErrorNo = "";
                ec.ErrorDescription = message;
                ecn.Add(ec);
                apiResponse = new ResponseTrackerContext()
                {
                    Errors = ecn,
                    Status = false
                };
                httpResponse = Request.CreateResponse(apiResponse);
                return httpResponse;
                //return Request.CreateErrorResponse(HttpStatusCode.NotFound, message);
            }
            List<Tracking> entities = new List<Tracking>();// = trackEntity.ToList<Tracking>();
            foreach (var x in trackEntity)
            {
                if (!(string.IsNullOrEmpty(x.ImagePath)))
                {
                    x.ReceivedImage =
                        System.IO.File.Exists(ConfigurationManager.AppSettings["PODImage"] + x.ImagePath.Replace(@"/", @"\")) ?
                        Convert.ToBase64String(System.IO.File.ReadAllBytes
                      (ConfigurationManager.AppSettings["PODImage"] + x.ImagePath.Replace(@"/", @"\"))) : "";
                }
                entities.Add(x);
            }
            //entities.ForEach(x =>
            //{
            //    if (!(string.IsNullOrEmpty(x.ImagePath)))
            //    {
            //        x.ReceivedImage =
            //            File.Exists(ConfigurationManager.AppSettings["PODImage"] + x.ImagePath.Replace(@"/", @"\")) ?
            //            Convert.ToBase64String(System.IO.File.ReadAllBytes
            //          (ConfigurationManager.AppSettings["PODImage"] + x.ImagePath.Replace(@"/", @"\"))) : "";
            //    }
            //});
            //TrackResultEntity trackResultEntity = new TrackResultEntity()
            //{
            //    status_code = ServiceConstants.CodeSuccess,
            //    trackingEntities = entities
            //    //response = new ResponseEntity()
            //    //{
            //    //    status_code = ServiceConstants.CodeSuccess
            //    //}
            //};
            apiResponse = new ResponseTrackerContext()
            {
                Status = true,
                TrackingDetails = entities
            };
            httpResponse = Request.CreateResponse(apiResponse);
            return httpResponse;
        }
        #endregion
        ///<summary>
        ///Error Handlers
        ///</summary>
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet, HttpPost, HttpPut, HttpDelete, HttpHead, HttpOptions, AcceptVerbs("PATCH")]
        public HttpResponseMessage Handle404()
        {
            var responseMessage = new HttpResponseMessage(HttpStatusCode.NotFound);
            responseMessage.ReasonPhrase = "The requested resource is not found";
            return responseMessage;
        }
    }
}