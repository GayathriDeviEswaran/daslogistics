﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Logistics.BusinessAccessInterface;
using Logistics.Entity;
using Logistics.ExceptionHandling;
using System.Net;
using System.Web.Http.Description;
using System.Net.Http;

namespace Logistics.Controllers
{
    public class AccountTermsController : System.Web.Http.ApiController
    {
        private readonly IAccountTerms _AccountTerms;
        #region "GetAccountTerms"
        /// <summary>
        /// This service is used to Get Account Terms details.
        /// </summary>
        ///  
        /// <returns>Account terms details </returns>
        [HttpGet]
        [Route("GetAccountTerms")]
        [ResponseType(typeof(AccountTerms))]
        public HttpResponseMessage GetAccountTerms (AccountTerms data)
        {
            var _returnData = _AccountTerms.GetAccountTerms(data);
            HttpResponseMessage httpResponse = Request.CreateResponse(_returnData);
            return httpResponse;
        }

        #endregion
    }
}