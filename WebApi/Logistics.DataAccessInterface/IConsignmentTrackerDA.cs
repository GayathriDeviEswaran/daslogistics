﻿using Logistics.Entity;
using System;
using System.Collections.Generic;

namespace Logistics.DataAccessInterface
{
    public interface IConsignmentTrackerDA
    {
        IEnumerable<Tracking> GetInvoiceByConNote(Consignment consignment);
        IEnumerable<Tracking> GetPickupDetail(Consignment consignment);
        IEnumerable<Tracking> GetLinehaulDepartDetail(Consignment consignment);
        IEnumerable<Tracking> GetLinehaulArriveDetail(Consignment consignment);
        IEnumerable<Tracking> GetOnforwardDetail(Consignment consignment);
        IEnumerable<Tracking> GetItemDeliveryLoadDetail(Consignment consignment);
        IEnumerable<Tracking> GetItemDeliveredDetail(Consignment consignment);
        IEnumerable<Tracking> GetConsignmentTracker(Consignment consignment);
    }
}
