﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logistics.Entity;
namespace Logistics.DataAccessInterface
{
    public interface ICommonExceptionDA
    {
        void AddErrorlog(CommonErrorException data);
        void Writelog(CommonErrorException data);
    }
}
