﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logistics.Entity;

namespace Logistics.DataAccessInterface
{
    public interface INotificationDA
    {
        
        IEnumerable<NotificationTemplate> GetNotificationByProcessID(Notification ProcessID);
        IEnumerable<NotificationTemplate> GetNotificationByID(Notification ID);

        IEnumerable<NotificationTemplate> AddNotification(NotificationTemplate data);

    }
}
