﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logistics.Entity;
namespace Logistics.DataAccessInterface
{
    public interface ISubrupsDA
    {
        //IEnumerable<SubrupsEntity> GetSubrupsDetails(SubrupsEntity data);
        IEnumerable<SubrupsEntity> GetSubrupsDetailsbyID(SubrupsID ID);
    }
}
