﻿using Logistics.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistics.DataAccessInterface
{
    public interface ILoginDA
    {
        ResponseLogin ValidateLogin(string username,string password);
      //  IEnumerable<MenuMaster> GetMenus(string Username, string Token);
    }
}
