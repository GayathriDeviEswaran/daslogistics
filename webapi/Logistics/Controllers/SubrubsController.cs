﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Web.Http.Description;
using System.Net.Http;
using System.Web.Mvc;
using Logistics.BusinessAccessInterface;
using Logistics.Entity;
using Logistics.ExceptionHandling;

namespace Logistics.Controllers
{
    public class SubrubsController : System.Web.Http.ApiController
    {
        private readonly ISubrubs _subrubslog;
        #region "GetSubrubsDetails"
        /// <summary>
        /// This service is used to return the Subrubs details based on ID.
        /// </summary>
        /// <param name="ID"><see cref="ApiDescription"/></param>
        /// <returns>Subrubs details </returns>
        [HttpGet]
        [Route("GetSubrubsByID")]
        [ResponseType(typeof(SubrupsEntity))]
        public HttpResponseMessage GetSubrupsDetailsbyID(SubrupsID ID)
        {
            var _returnData = _subrubslog.GetSubrupsDetailsbyID(ID);
            HttpResponseMessage httpResponse = Request.CreateResponse(_returnData);  
            return httpResponse;
        }

        #endregion

    }
}