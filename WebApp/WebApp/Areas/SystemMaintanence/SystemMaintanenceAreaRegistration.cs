﻿using System.Web.Mvc;

namespace WebApp.Areas.SystemMaintanence
{
    public class SystemMaintanenceAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "SystemMaintanence";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "SystemMaintanence_default",
                "SystemMaintanence/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}